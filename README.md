## Some packages for OpenWRT

### Howto

Copy feeds.conf.default as feeds.conf if necessary and add this line at the end

```
src-git pkgx https://gitlab.com/iotvng/owrt-pkgx.git
```

Update the newly added feed and install its packages and you're good to go

```
scripts/feeds update -a
scripts/feeds install -p pkgx -a
```

Now the packages will be appeared in menuconfig for selection